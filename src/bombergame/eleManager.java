/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bombergame;

import java.util.Random;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;

/**
 *
 * @author Rongan
 */
public class eleManager {
    ImageView bomber;
    Image bomberPic;
    
    public eleManager(){
        bomber = new ImageView();
        bomberPic =  new Image("file:../../images/bomber.png");
        bomber.setImage(bomberPic);
    }

    
    public ImageView getBomber(){
        return bomber;
    }
}
