/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bombergame;

import java.util.Random;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

/**
 *
 * @author Rongan
 */
public class terroristGenerator extends Thread{
    Pane root;
    
    @Override
    public void run() {
            for(int i=0; i<40; i++) {
                ImageView ISISImage = terrorist();
                addTerrorist(root,ISISImage);
                // Sleep for a while
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    // Interrupted exception will occur if
                    // the Worker object's interrupt() method
                    // is called. interrupt() is inherited
                    // from the Thread class.
                    break;
            }
        }
    }
    
    public void setRoot(Pane root){
        this.root = root;
    }
    
    public ImageView terrorist(){
        ImageView terrorist = new ImageView();
        Image terroristPic =  new Image("file:../../images/terrorist.jpg");
        terrorist.setImage(terroristPic);
        Random rand = new Random();
        int x_coordinate = rand.nextInt((450-150)+1) + 150;
        int Y_coordinate = rand.nextInt((450-150)+1) + 150;
        terrorist.setX(x_coordinate);
        terrorist.setY(Y_coordinate);
        return terrorist;
    }
    
    public void addTerrorist(Pane root,ImageView image){
        root.getChildren().add(image);
    }
        
}

