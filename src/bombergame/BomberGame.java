/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bombergame;


import javafx.animation.PathTransition;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.VLineTo;
import javafx.stage.Stage;
import javafx.util.Duration;


/**
 *
 * @author Rongan
 */
public class BomberGame extends Application {
    ImageView bomber;
    
    @Override
    public void start(Stage primaryStage) {
        eleManager elManager = new eleManager();
        bomber = elManager.getBomber();
        Pane root = new Pane();
        root.getChildren().addAll(bomber);
        
        terroristGenerator terrGene = new terroristGenerator();
        terrGene.setRoot(root);
        terrGene.run();
        
        Scene scene = new Scene(initPane.getInitPane(), 900, 550); 
        primaryStage.setTitle("bomb ISIS");
        primaryStage.setScene(scene);
        primaryStage.show();
        
    
        
        scene.setOnKeyPressed(e->{
            if(e.getCode() == KeyCode.SPACE){
                dropBomb(root);               
            }
            move(e.getCode());
        });
        
        initPane.getOkButton().setOnAction(e->{
            scene.setRoot(root);
        });
    }
    
    
    /**
     * the bombFall method makes the bomb fall to the ground while it's 
     */
    private void bombFall(ImageView bomb){       
        Path path = new Path();
        path.getElements().add(new MoveTo(bomb.getX(),100));
        path.getElements().add(new VLineTo(1000));
        PathTransition pathTransition = new PathTransition();
        pathTransition.setDuration(Duration.millis(4000));
        pathTransition.setPath(path);
        pathTransition.setNode(bomb);
        pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
        pathTransition.setAutoReverse(false);
        pathTransition.play();
    }
    
    
    private void move(KeyCode key){
        if(key == KeyCode.RIGHT){
            Image right_bomber = new Image("file:../../images/bomber.png");
            bomber.setImage(right_bomber);
            bomber.setX(bomber.getX()+3);
        }
        else if(key == KeyCode.LEFT){
            Image flipped_bomber = new Image("file:../../images/flipped_bomber.png");
            bomber.setImage(flipped_bomber);
            bomber.setX(bomber.getX()-3);
        }
        else if(key == KeyCode.UP){
            bomber.setY(bomber.getY() - 3);
        }
        else if(key == KeyCode.DOWN){
            bomber.setY(bomber.getY() + 3);
        }
    }
    
    
    
    private ImageView createBomb(){
            double bombInitX = bomber.getX();
            double bombInitY = bomber.getY();
            Image bomb = new Image("file:../../images/bomb.png");
            ImageView bombImg = new ImageView(bomb);
            bombImg.setX(bombInitX);
            bombImg.setY(bombInitY);
            return bombImg;
    }

    private void dropBomb(Pane root){
        ImageView bomb = createBomb();
        bombFall(bomb);
        root.getChildren().add(bomb);
        }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
