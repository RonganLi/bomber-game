/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bombergame;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 *
 * @author Rongan
 */
public class initPane {
    static Label dir;
    static Button okButton; 
    static VBox initialPane;
    

    
    public static VBox getInitPane(){
        dir = new Label("Instruction: /nUse arrow keys to move the bomber, and space to drop bomb.");
        okButton = new Button("OK");
        initialPane = new VBox();
        initialPane.getChildren().addAll(dir,okButton);
        return initialPane;
    }
    
    public static Button getOkButton(){
        return okButton;
    }
}
